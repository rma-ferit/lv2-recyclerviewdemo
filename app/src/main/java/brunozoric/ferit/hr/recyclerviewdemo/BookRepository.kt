package brunozoric.ferit.hr.recyclerviewdemo

object BookRepository{
    val books: MutableList<Book>

    init {
        books = retrieveBooks()
    }

    private fun retrieveBooks(): MutableList<Book> {
        return mutableListOf(
            Book(1, "The Great Gatsby", "F. Scott Fitzgerald",
                "https://upload.wikimedia.org/wikipedia/en/f/f7/TheGreatGatsby_1925jacket.jpeg"),
            Book(2, "The Grapes of Wrath", "John Steinbeck",
                "https://upload.wikimedia.org/wikipedia/en/1/1f/JohnSteinbeck_TheGrapesOfWrath.jpg"),
            Book(3, "Nineteen Eighty-Four", "George Orwell",
                "https://upload.wikimedia.org/wikipedia/hr/7/7e/Orwell1984hrv.jpg"),
            Book(4, "It", "Stephen King",
            "https://upload.wikimedia.org/wikipedia/en/5/5a/It_cover.jpg"),
            Book(5, "The lord of the rings", "John R.R. Tolkien",
                "https://upload.wikimedia.org/wikipedia/en/e/e9/First_Single_Volume_Edition_of_The_Lord_of_the_Rings.gif")
        )
    }

    fun remove(id: Int) = books.removeAll{ book -> book.id == id }
    fun get(id: Int): Book? = books.find { book -> book.id == id }
    fun add(book: Book) = books.add(book)
}