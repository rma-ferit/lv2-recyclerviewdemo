package brunozoric.ferit.hr.recyclerviewdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_book.view.*

class BookAdapter(
    books: MutableList<Book>,
    bookListener: BookInteractionListener
): RecyclerView.Adapter<BookHolder>() {

    private val books: MutableList<Book>
    private val bookListener: BookInteractionListener

    init {
        this.books = mutableListOf()
        this.books.addAll(books)
        this.bookListener = bookListener
    }

    fun refreshData(books: MutableList<Book>) {
        this.books.clear()
        this.books.addAll(books)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = books.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookHolder {
        val bookView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_book, parent, false)
        val bookHolder = BookHolder(bookView)
        return bookHolder
    }

    override fun onBindViewHolder(holder: BookHolder, position: Int) {
        val book = books[position]
        holder.bind(book, bookListener)
    }
}

class BookHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(book: Book, bookListener: BookInteractionListener) {
        itemView.itemBookTitle.text = book.title
        itemView.itemBookAuthor.text = book.author
        Picasso.get()
            .load(book.cover)
            .fit()
            .placeholder(R.drawable.ic_book)
            .error(android.R.drawable.stat_notify_error)
            .into(itemView.itemBookCover)
        itemView.setOnClickListener { bookListener.onShowDetails(book.id) }
        itemView.setOnLongClickListener{ bookListener.onRemove(book.id); true; }
    }
}
