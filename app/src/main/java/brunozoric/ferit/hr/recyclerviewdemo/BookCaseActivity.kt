package brunozoric.ferit.hr.recyclerviewdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_book_case.*

class BookCaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_case)
        setUpUi()
    }

    private fun setUpUi() {
        booksDisplay.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        booksDisplay.itemAnimator = DefaultItemAnimator()
        booksDisplay.addItemDecoration(DividerItemDecoration(this,RecyclerView.VERTICAL))
        displayData()
    }

    private fun displayData() {
        val bookListener = object: BookInteractionListener{
            override fun onRemove(id: Int) {
                BookRepository.remove(id)
                (booksDisplay.adapter as BookAdapter).refreshData(BookRepository.books)
            }

            override fun onShowDetails(id: Int) {
                val book = BookRepository.get(id)
                Toast.makeText(applicationContext, book.toString(), Toast.LENGTH_SHORT).show()
            }
        }
        booksDisplay.adapter = BookAdapter(BookRepository.books, bookListener)
    }
}
