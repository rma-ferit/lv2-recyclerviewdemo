package brunozoric.ferit.hr.recyclerviewdemo

interface BookInteractionListener{
    fun onRemove(id: Int)
    fun onShowDetails(id: Int)
}