package brunozoric.ferit.hr.recyclerviewdemo

data class Book (
    val id: Int = 0,
    val title: String,
    val author: String,
    val cover: String){
}